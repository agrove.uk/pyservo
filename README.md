# PyServo

Notes for Raspberry PI project to control servos from Python

This is a good place to [start](https://gpiozero.readthedocs.io/en/stable/index.html)

## Pins

There are two ways to define these:

- [] the physical pin (board) 
- [] the GPIO numbers.

e.g.

```python
import RPi.GPIO as GPIO  
  
# for GPIO numbering, choose BCM  
GPIO.setmode(GPIO.BCM)  
  
# or, for pin numbering, choose BOARD  
GPIO.setmode(GPIO.BOARD)  
```

For the Raspberry Pi 400 the following is the best reference I found from [here](https://pi4j.com/getting-started/understanding-the-pins/)

![image info](images/headerpins_p400.png)

## Controlling the servo

## Jitter on the servo

There is an alternative way to create the PWM using the see [here](https://ben.akrin.com/raspberry-pi-servo-jitter/)


```bash
sudo apt-get update && sudo apt-get install python3-pigpio
sudo pigpiod
```